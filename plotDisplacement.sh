#! /bin/bash
gnuplot -p << EOF
set grid
set title 'Displacement of the wing Tip'
set xlabel 'Time [s]'
set ylabel 'Y-Displacement [m]'
# set style line 1 lt 2 lw 6
# set style line 1 lt 2 lw 6
# set linestyle  2 lt 2 lc 1 # red-dashed
# set linestyle  1 lt 2 lc 1 # red-dashed
plot "./precice-Solid-watchpoint-tip-leading.log" using 1:9 title "tip lead" with lines, \
     "./precice-Solid-watchpoint-tip.log" using 1:9 title "tip" with lines, \
     "./precice-Solid-watchpoint-tip-trailing.log" using 1:9 title 'tip trail' with lines, \
     "./precice-Solid-watchpoint-root.log" using 1:9 title 'root' with lines, \
     "./precice-Solid-watchpoint-mid-leading.log" using 1:9 title "mid lead" with lines, \
     "./precice-Solid-watchpoint-tip-trailing.log" using 1:9 title 'mid trail' with lines, \
     "./precice-Solid-watchpoint-mid.log" using 1:9 title 'mid' with lines
EOF

