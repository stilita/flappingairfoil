# $Header: /var/cvs/mbdyn/mbdyn/mbdyn-1.0/tests/forces/strext/socket/simplerotor/mapping/simplerotor2_mapping,v 1.7 2017/01/12 15:02:57 masarati Exp $
#
# MBDyn (C) is a multibody analysis code. 
# http://www.mbdyn.org
# 
# Copyright (C) 1996-2017
# 
# Pierangelo Masarati	<masarati@aero.polimi.it>
# Paolo Mantegazza	<mantegazza@aero.polimi.it>
# 
# Dipartimento di Ingegneria Aerospaziale - Politecnico di Milano
# via La Masa, 34 - 20156 Milano, Italy
# http://www.aero.polimi.it
# 
# Changing this copyright notice is forbidden.
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation (version 2 of the License).
# 
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Author: Giuseppe Quaranta <quaranta@aero.polimi.it>


begin: data;
	problem: initial value;
end: data;


include: "heathcote.set";


begin: initial value;

	initial time: INITIAL_TIME;
	final time: FINAL_TIME;
	time step: DT;
#	set: const integer TIMESTEP = 1234;
#	strategy: change, postponed, TIMESTEP; # changes the time step as specified by the drive caller labeled TIMESTEP, whose definition is postponed (this is simply a placeholder)

	method: ms, .6;
	#method: bdf;
	nonlinear solver: newton raphson, modified, 5;
	linear solver: umfpack, colamd, mt, 1;
#	linear solver: naive, colamd;
#    linear solver: umfpack;

	tolerance: 1e-6;
	max iterations: 1000;

	derivatives coefficient: 1e-9;
	derivatives tolerance: 1e-6;
	derivatives max iterations: 100;

	output: iterations;
	output: residual;
end: initial value;



begin: control data;
	structural nodes: 
		+1    # clamped node
		+2*N  # other nodes
	;
	rigid bodies:
		+2*N+1  # mass of  nodes
	;
	joints:
		+1    # clamp
		#+2*N  # other total joints on nodes to force 2D
	;
	beams:
		+N    # the whole beam
	;
	forces:
		+1    # loads the beam
#		+1    # load on last node
	;
#	file drivers:
#	    +1
#	;
end: control data;


drive caller: WING_EXCITATION,
	mult,
		# 1.,
		# initial time, angular velocity, amplitude, num cycles, initial value
		# f (t) = initial_value+ amplitude * (1 − cos (angular_velocity * (t − initial_time))).
		cosine, T_START, COS_OMEGA, .5, half, 0.,
		
		# initial time, angular velocity, amplitude, num cycles, initial value		
		#f(t) = initial_value+ amplitude · sin (angular_velocity · (t − initial_time))
		sine, T_START, EXCITATION_OMEGA, EXCITATION_AMPL_FACTOR*CHORD, forever, 0.;


set: E = E_STEEL;
set: NU = NU_STEEL;
set: RHO = RHO_STEEL;

#set: E = E_ALUMINUM;
#set: NU = NU_ALUMINUM;
#set: RHO = RHO_ALUMINUM;


reference: GROUND,
	reference, global, null,
	reference, global, eye,
	reference, global, null,
	reference, global, null;


include: "heathcote.ref";


begin: nodes;
	structural: ROOT, dynamic,
		reference, ROOT, null,		# position
		reference, ROOT, eye,		# orientation
		reference, ROOT, null,		# initial velocity
		reference, ROOT, null;		# angular velocity
	
	set: curr_node = 2;
	include: "beam.nod";
	set: curr_node = 4;
	include: "beam.nod";
	set: curr_node = 6;
	include: "beam.nod";
	set: curr_node = 8;
	include: "beam.nod";
	set: curr_node = 10;
	include: "beam.nod";
	set: curr_node = 12;
	include: "beam.nod";
	set: curr_node = 14;
	include: "beam.nod";
	set: curr_node = 16;
	include: "beam.nod";
	set: curr_node = 18;
	include: "beam.nod";
	set: curr_node = 20;
	include: "beam.nod";

end: nodes;

#begin: drivers;
#	set: const integer INPUT = 200;
#	file: INPUT, stream,
#		  stream drive name, "TS_DRV", # irrelevant but needed
#		  create, yes,
#		  path, "/tmp/mbdyn.ts.sock", # or use port
#		  1;      # one channel: the time step
#	  drive caller: TIMESTEP, file, INPUT, 1; # replace placeholder with file driver
#end: drivers;





begin: elements;
	#joint: 500+ROOT, clamp, ROOT, node, node;
	
	joint: 500+ROOT, total pin joint,
	ROOT,										# node
	position, reference, ROOT, null,            # relative offset
	position, reference, ROOT, null,
	#rotation orientation, eye, # relative rotation orientation
	position constraint, active, active, active,
		0., 1., 0., reference, WING_EXCITATION,
	orientation constraint, active, active, active,
		null;
	
	include: "first_body.elm";
	set: curr_node = 2;
	include: "beam.elm";
	set: curr_node = 4;
	include: "beam.elm";
	set: curr_node = 6;
	include: "beam.elm";
	set: curr_node = 8;
	include: "beam.elm";
	set: curr_node = 10;
	include: "beam.elm";
	set: curr_node = 12;
	include: "beam.elm";
	set: curr_node = 14;
	include: "beam.elm";
	set: curr_node = 16;
	include: "beam.elm";
	set: curr_node = 18;
	include: "beam.elm";
	set: curr_node = 20;
	include: "last_beam.elm";
	    
    force: CURR_BEAM, external structural mapping,
		socket,
		create, yes,
		    path, "/tmp/mbdyn2.node.sock",
		    no signal,
		    coupling,
			    # loose,
			    tight,
		#reference node, 1,
		#orientation, euler 123,
		#use reference node forces, yes,
		points number, 3 * BEAM_NNODES,
			CURR_BEAM, 
				offset, null,
				offset, da, 0., 0.,
				offset, 0., da, 0.,
			CURR_BEAM +  1, 
				offset, null,
				offset, da, 0., 0.,
				offset, 0., da, 0.,
			CURR_BEAM +  2, 
				offset, null,
				offset, da, 0., 0.,
				offset, 0., da, 0.,
			CURR_BEAM +  3, 
				offset, null,
				offset, da, 0., 0.,
				offset, 0., da, 0.,
			CURR_BEAM +  4,
				offset, null,
				offset, da, 0., 0.,
				offset, 0., da, 0.,
			CURR_BEAM +  5,
				offset, null,
				offset, da, 0., 0.,
				offset, 0., da, 0.,
			CURR_BEAM +  6, 
				offset, null,
				offset, da, 0., 0.,
				offset, 0., da, 0.,
			CURR_BEAM +  7, 
				offset, null,
				offset, da, 0., 0.,
				offset, 0., da, 0.,
			CURR_BEAM +  8, 
				offset, null,
				offset, da, 0., 0.,
				offset, 0., da, 0.,
			CURR_BEAM +  9, 
				offset, null,
				offset, da, 0., 0.,
				offset, 0., da, 0.,
			CURR_BEAM + 10,
				offset, null,
				offset, da, 0., 0.,
				offset, 0., da, 0.,
			CURR_BEAM +  11, 
				offset, null,
				offset, da, 0., 0.,
				offset, 0., da, 0.,
			CURR_BEAM +  12, 
				offset, null,
				offset, da, 0., 0.,
				offset, 0., da, 0.,
			CURR_BEAM +  13, 
				offset, null,
				offset, da, 0., 0.,
				offset, 0., da, 0.,
			CURR_BEAM +  14,
				offset, null,
				offset, da, 0., 0.,
				offset, 0., da, 0.,
			CURR_BEAM +  15,
				offset, null,
				offset, da, 0., 0.,
				offset, 0., da, 0.,
			CURR_BEAM +  16, 
				offset, null,
				offset, da, 0., 0.,
				offset, 0., da, 0.,
			CURR_BEAM +  17, 
				offset, null,
				offset, da, 0., 0.,
				offset, 0., da, 0.,
			CURR_BEAM +  18, 
				offset, null,
				offset, da, 0., 0.,
				offset, 0., da, 0.,
			CURR_BEAM +  19, 
				offset, null,
				offset, da, 0., 0.,
				offset, 0., da, 0.,
			CURR_BEAM + 20,
				offset, null,
				offset, da, 0., 0.,
				offset, 0., da, 0.,
				
		#echo, "naca0012_surface_points.dat", surface, "naca0012_surface.dat", output, "naca0012_surface_H.dat", order, 2, basenode, 12, weight, 2, stop;
		mapped points number, 1915,
		sparse mapping file, "naca0012_surface_H.dat";

# constant absolute force in node 21
#	force: 2, absolute, 
#		2*N + 1,
#		position,  null,
#		0., 1., 0.,
#		# slope, initial time, final time / forever, initial value
#		ramp, 0., 0., 1., 0.;
end: elements;


# vim:ft=mbd
