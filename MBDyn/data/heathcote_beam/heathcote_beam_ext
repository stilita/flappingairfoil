# MBDyn (C) is a multibody analysis code. 
# http://www.mbdyn.org
# 
# Copyright (C) 1996-2010
# 
# Pierangelo Masarati	<masarati@aero.polimi.it>
# Paolo Mantegazza	<mantegazza@aero.polimi.it>
# 
# Dipartimento di Ingegneria Aerospaziale - Politecnico di Milano
# via La Masa, 34 - 20156 Milano, Italy
# http://www.aero.polimi.it
# 
# Changing this copyright notice is forbidden.
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation (version 2 of the License).
# 
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Author: Pierangelo Masarati <masarati@aero.polimi.it>
# From: Heathcote, S., Wang, Z.-J., and Gursul, I.,
# "Effect of Spanwise Flexibility on Flapping Wing Propulsion,"
# 36th AIAA Fluid Dynamics Conference, San Francisco, California,
# June 5­8 2006, AIAA-2006-2870.

begin: data;
	problem: initial value;
end: data;

begin: initial value;
	initial time: 0.;
	final time: 2.;
	time step: 2e-3;

	tolerance: 1e-4;
	max iterations: 20;

	method: ms, .6;

	output: iterations;
end: initial value;

include: "heathcote_beam_m.set";

begin: control data;
	structural nodes:
		+ NNODES
	;
	rigid bodies:
		+ NBODIES
	;
	joints:
		+ NJOINTS
	;
	beams:
		+ NBEAMS
	;
	forces:
		+ 1		# external force
	;
end: control data;

drive caller: WING_EXCITATION,
	mult, cosine, 0., pi/.01, .5, half, 0.,
		sine, 0., 2*pi/1., 0.175*CHORD, forever, 0.;

set: E = E_STEEL;
set: NU = NU_STEEL;
set: RHO = RHO_STEEL;

#set: E = E_ALUMINUM;
#set: NU = NU_ALUMINUM;
#set: RHO = RHO_ALUMINUM;

set: WING_DAMP = 1e-4;

include: "heathcote_beam_m.ref";

begin: nodes;
	include: "heathcote_beam_m.nod";
end: nodes;

begin: elements;
	include: "heathcote_beam_m.elm";

	force: 111, external structural mapping,
		socket,
		create, yes,
		path, "/tmp/mbdyn_overture.sock",
		no signal,
		coupling,
			# loose,
			tight,
		points number, 4*11,
			0, offset, .5*CHORD, 0., 0., offset, -.5*CHORD, 0., 0., offset, 0., .05*CHORD, 0., offset, 0., -.05*CHORD, 0.,
			1, offset, .5*CHORD, 0., 0., offset, -.5*CHORD, 0., 0., offset, 0., .05*CHORD, 0., offset, 0., -.05*CHORD, 0.,
			2, offset, .5*CHORD, 0., 0., offset, -.5*CHORD, 0., 0., offset, 0., .05*CHORD, 0., offset, 0., -.05*CHORD, 0.,
			3, offset, .5*CHORD, 0., 0., offset, -.5*CHORD, 0., 0., offset, 0., .05*CHORD, 0., offset, 0., -.05*CHORD, 0.,
			4, offset, .5*CHORD, 0., 0., offset, -.5*CHORD, 0., 0., offset, 0., .05*CHORD, 0., offset, 0., -.05*CHORD, 0.,
			5, offset, .5*CHORD, 0., 0., offset, -.5*CHORD, 0., 0., offset, 0., .05*CHORD, 0., offset, 0., -.05*CHORD, 0.,
			6, offset, .5*CHORD, 0., 0., offset, -.5*CHORD, 0., 0., offset, 0., .05*CHORD, 0., offset, 0., -.05*CHORD, 0.,
			7, offset, .5*CHORD, 0., 0., offset, -.5*CHORD, 0., 0., offset, 0., .05*CHORD, 0., offset, 0., -.05*CHORD, 0.,
			8, offset, .5*CHORD, 0., 0., offset, -.5*CHORD, 0., 0., offset, 0., .05*CHORD, 0., offset, 0., -.05*CHORD, 0.,
			9, offset, .5*CHORD, 0., 0., offset, -.5*CHORD, 0., 0., offset, 0., .05*CHORD, 0., offset, 0., -.05*CHORD, 0.,
			10, offset, .5*CHORD, 0., 0., offset, -.5*CHORD, 0., 0., offset, 0., .05*CHORD, 0., offset, 0., -.05*CHORD, 0.,
		# echo, "hb_mb.dat", precision, 16, surface, "heathcote_surfaceGrid.dat", output, "heathcote_beam_H.dat", order, 2, basenode, 12, weight, 2, stop;
		mapped points number, from file,
		sparse mapping file, "heathcote_beam_H.dat";
end: elements;

# vim:ft=mbd
